<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('cors')->get('users', 'ApiTicketController@userIndex');
Route::middleware('cors')->get('tickets', 'ApiTicketController@index');
Route::middleware('cors')->get('tickets/{ticket}', 'ApiTicketController@show');
Route::middleware('cors')->put('tickets/{ticket}', 'ApiTicketController@update');
Route::middleware('cors')->post('tickets/{ticket}', 'ApiTicketController@store');
Route::middleware('cors')->delete('tickets/{ticket}', 'ApiTicketController@destroy');
