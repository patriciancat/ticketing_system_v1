<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchFormRequest;
use App\Http\Requests\TicketFormRequest;
use Illuminate\Http\Request;
use App\Ticket;
use App\User;
use App\Comment;

class TicketController extends Controller
{
    public function create()
    {
        $ticket = new Ticket;
        return view('tickets.create', ['ticket' => $ticket ]);
    }

    public function index(Request $request)
    {
        $tickets = Ticket::with('ticket_comments')->orderBy('id', 'ASC')->paginate(4);
        return view('tickets.index',compact('tickets')) ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function show($id)
    {
        $ticket = Ticket::with('ticket_comments')->find($id);
        return view('tickets.show', ['ticket' => $ticket]);
    }




    public function edit($id)
    {
        $ticket= Ticket::find($id);
        return view('tickets.edit',compact('ticket'));
    }



    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
        ]);
        Ticket::find($id)->update($request->all());
        return redirect()->route('tickets.show', $id) ->with('success','Ticket status updated successfully');
    }

    public function destroy($id)
    {
        Ticket::find($id)->delete();
        return redirect()->route('tickets.index') ->with('success','Ticket deleted successfully');
    }

    public function store(TicketFormRequest $request)
    {
        $allRequest = $request->all();

        $ticket = new Ticket();
        $ticket->os = $allRequest['os'];
        $ticket->summary = $allRequest['summary'];
        $ticket->user_id = auth()->user()->id;
        $ticket->status = "Pending";
        $ticket->save();

        $comment = new Comment();
        $comment->comment = $allRequest['comment'];
        $comment->ticket_id = $ticket->id;
        $comment->save();

        $request->session()->put('ticket', $ticket);
        $request->session()->put('user', auth()->user()->name);

        return redirect()->route('tickets.show', $ticket->id) ->with('success','Ticket added
successfully');
    }

    public function search(SearchFormRequest $request)
    {
        $ticket_id = $request->only(['search']);
        $ticket = Ticket::find($ticket_id)->first();
        if($ticket==null)
        {
            return redirect()->route('search') ->withErrors(['Ticket not found!']);
        }
        else
        {
            return redirect()->route('tickets.show', $ticket->id) ->with('success', 'Ticket found!');
        }



    }
}
