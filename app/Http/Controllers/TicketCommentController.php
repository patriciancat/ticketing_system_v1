<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\Comment;
use App\User;
use App\Http\Requests\CommentFormRequest;

class TicketCommentController extends Controller
{
    public function index(Request $request, $ticket)
    {

        $comments = Comment::where('ticket_id', $ticket)->paginate(4);
        $currentTicket = Ticket::find($ticket);
        return view('comments.index', ['ticket' => $currentTicket, 'comments' => $comments])->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create($ticket)
    {
        $currentTicket = Ticket::find($ticket);
        $comment = new Comment();
        return view('comments.create', ['ticket' => $currentTicket, 'comment' => $comment ]);
    }

    public function store(CommentFormRequest $request, $ticket)
    {
        $allRequest = $request->all();
        $currentTicket = Ticket::find($ticket);

        $comment = new Comment();
        $comment->comment = $allRequest['comment'];
        $comment->ticket_id = $currentTicket->id;
        $comment->save();

        $request->session()->put('ticket', $currentTicket);
        $request->session()->put('comment', $comment->comment);

        return redirect()->route('tickets.comments.create', ['ticket' => $ticket]) ->with('success','Comment added
        successfully');
    }

}
