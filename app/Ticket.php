<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'os', 'summary', 'status',
    ];

    public function user_id() {
        return $this->belongsTo('App\User');
    }

    public function ticket_comments()
    {
        return $this->hasMany('App\Comment');
    }
}
