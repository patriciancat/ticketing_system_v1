<!DOCTYPE html>
<html lang="en">

<body>
    <header>
    @extends('master')
    @section('title', 'Home')
    @section('header')
    <!--Mask-->
    <div class="view hm-black-light">
        <div class="full-bg-img flex-center">
            <div class="container">
                <ul>
                    <li>
                        <h1 class="h1-responsive wow fadeInDown">ITS Ticketing System</h1>
                    </li>
                    <li>
                        <p class="wow fadeInDown" data-wow-delay="0.2s">Welcome to the ITS Ticket Submission system!
                            If you are experiencing difficulties with your IT system, please submit a ticket and the team will attempt to resolve your issue as soon as possible.</p>
                    </li>
                    @if (Auth::guest())
                        <li>
                            <a href="{{ route('login') }}" class="btn btn-primary btn-lg wow fadeInLeft" data-wow-delay="0.4s">Login</a>
                            <a href="{{ route('register') }}" class="btn btn-info btn-lg wow fadeInRight" data-wow-delay="0.4s">Register</a>
                        </li>
                    @else
                        <li>
                            <a href="{{ route('tickets.create') }}" class="btn btn-primary btn-lg wow fadeInUp" data-wow-delay="0.4s">Submit a Ticket</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    <!--/.Mask-->




    </header>
    @section('content')

    <main class="text-center">
        <div class="container">

            <h2 class="h2-responsive my-5">Frequently Asked Questions</h2>

            <!--Section: FAQ -->
            @include('faq')
            <h2 class="h2-responsive my-5">Contact us</h2>

            <!--Section: contact-->
            @include('contact')


        </div>
    </main>


</body>

</html>
