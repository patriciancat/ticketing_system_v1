@extends('layouts/default')

<main>
    @section('content')
        <div class="container m-5">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12 mt-5 text-center">
                    <div class="text-center">
                        <h2>Ticket {{$ticket->id}}</h2>
                    </div>
                </div>


                    <div class="col-md-8 offset-md-2">
                        <!--Card-->
                        <div class="card hoverable">
                            <!--Card content-->
                            <div class="card-body">
                                <!--Title-->
                                <p class="card-text text-center">Operating System: </p>
                                <p class="card-block text-center">{{$ticket->os}}</p>
                                <hr>
                                <p class="card-text text-center">Issue summary: </p>
                                <p class="card-block text-center">{{$ticket->summary}}</p>
                                <hr>
                                <p class="card-text text-center">Comments:  </p>
                                @foreach($ticket->ticket_comments as $comment)
                                    <p class="card-block text-center">{{$comment->comment}}</p><hr>
                                @endforeach
                                <p class="card-text text-center">Status: </p>
                                <p class="card-block text-center">{{$ticket->status}}</p>
                                <hr>
                                <div class="text-center">
                                    <div class="d-inline">
                                        <a href="{{route('home')}}" class="btn btn-primary align-content-center" >Return to Home</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
            </div>
        </div>
</main>