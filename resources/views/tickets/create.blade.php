@extends('layouts/default')
<main>
    @section('content')
    <div class="container m-5">
        <div class="row">
            <div class="col-md-12 mt-5 text-center">
                <div class="pull-left">
                    <h2>Add New Ticket</h2>
                </div>
            </div>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        {!! Form::model($ticket, ['action' => 'TicketController@store']) !!}

        <div class="row">
            <div class="col-md-12">
                <h4>Enter ticket details:</h4>
            </div>

            <div class="col-md-6 form-group">
                {!! Form::label('os', 'OS') !!}
                {!! Form::text('os', '', ['class' => 'form-control']) !!}
            </div>
            <div class="col-md-6 form-group">
                {!! Form::label('summary', 'Issue Summary') !!}
                {!! Form::text('summary', '', ['class' => 'form-control']) !!}
            </div>
            <div class="col-md-6 form-group">
                {!! Form::label('comment', 'Comment') !!}
                {!! Form::text('comment', '', ['class' => 'form-control']) !!}
            </div>

        </div>

        <button class="btn btn-success" type="submit">Submit Ticket</button>

        {!! Form::close() !!}
    </div>
</main>