@extends('layouts/default')

<main>
    @section('content')
    <div class="container m-5">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12 mt-5 text-center">
                <div class="pull-left">
                    <h2>All Tickets</h2>
                </div>
            </div>

            @if($tickets->count()==0)
                <div class="col-md-8 offset-md-2" style="height: 38%;">
                    <h3 class="text-center">There are no tickets.</h3>
                </div>
            @endif

            @foreach ($tickets as $ticket)
            <div class="col-md-3 text-center">
                <!--Card-->
                <div class="card hoverable">
                    <!--Card content-->
                    <div class="card-body">
                        <!--Title-->
                        <h4 class="card-title text-center">Ticket {{$ticket->id}}</h4>
                        <h6 class="text-center">Operating System: {{$ticket->os}}</h6>
                        <hr>
                        <p class="card-text text-center">Issue summary: {{$ticket->summary}}</p>
                        <hr>
                        <p class="card-text text-center">Comments:
                        @if($ticket->ticket_comments->count()==0)
                            There are no comments.
                        @else
                            {{$ticket->ticket_comments->first()->comment}} </p>
                            @if($ticket->ticket_comments->count()>1)
                            ... <p class="card-text text-center">Click 'View Ticket' to see all comments</p>
                            @endif
                        @endif
                        <hr>
                        <p class="card-text text-center">Status: {{$ticket->status}}</p>
                        <hr>
                        <div class="text-center">
                            <a class="btn btn-success align-content-center" href="tickets/{{$ticket->id}}">View Ticket</a>
                        </div>
                        {!! Form::open(['method' => 'DELETE','route' => ['tickets.destroy', $ticket->id],'style'=>'text-center']) !!}
                        {!! Form::submit('Delete Ticket', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="row">

            {!! $tickets->links() !!}
        </div>



        </div>


</main>
