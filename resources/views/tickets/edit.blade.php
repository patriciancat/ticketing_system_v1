@extends('layouts/default')
<main>
    @section('content')
        <div class="container m-5">
            <div class="row">
                <div class="col-md-12 mt-5 text-center">
                    <div class="text-center">
                        <h2>Ticket {{$ticket->id}}</h2>
                    </div>
                </div>


                <div class="col-md-8 offset-md-2">
                    <!--Card-->
                    <div class="card hoverable">
                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <p class="card-text text-center">Operating System: </p>
                            <p class="card-block text-center">{{$ticket->os}}</p>
                            <hr>
                            <p class="card-text text-center">Issue summary: </p>
                            <p class="card-block text-center">{{$ticket->summary}}</p>
                            <hr>
                            <p class="card-text text-center">Comments:  </p>
                            @foreach($ticket->ticket_comments as $comment)
                                <p class="card-block text-center">{{$comment->comment}}</p><hr>
                            @endforeach
                            {!! Form::model($ticket, ['method' => 'PATCH','route' => ['tickets.update', $ticket->id]]) !!}
                            <p class="card-text text-center">Ticket Status: </p>
                            <div class="text-center">
                                {!! Form::select('status', array('Pending' => 'Pending', 'In Progress' => 'In Progress', 'Unresolved' => 'Unresolved', 'Resolved' => 'Resolved'), 'Pending'); !!}
                            </div>
                            <button type="submit" class="btn btn-success pull-right">Save Ticket</button>
                            <a class="btn btn-warning pull-left" href="{{route('tickets.index')}}">Cancel Edit</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
</main>