<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>RMIT ITS Ticketing System</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="../resources/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="../resources/assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="../resources/assets/css/style.css" rel="stylesheet">
</head>

<body>

    @include('shared.navbar')
    @include('shared.footer')
    @yield('header')
    @yield('content')

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="../resources/assets/js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="../resources/assets/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="../resources/assets/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="../resources/assets/js/mdb.min.js"></script>


    <!--Animations initialisation-->
    <script>
        new WOW().init();
    </script>

    <!--Panel Slide Stuff -->
    <script>
        $(document).ready(function () {
            $(".flip").click(function () {
                $($(this).nextAll(".panel")[0]).slideToggle("slow");
            });
        });
    </script>
    <style type="text/css">
        .panel {
            padding: 40px;
            display: none;
        }

        h3 {
            padding-left: 20px;

        }
    </style>

</body>


</html>