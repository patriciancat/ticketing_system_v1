@extends('layouts/default')

<main>
    @section('content')
        <div class="container m-5">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12 mt-5 text-center">
                    <div class="text-center">
                        <h2>Ticket {{$ticket->id}}</h2>
                    </div>
                </div>


                <div class="col-md-8 offset-md-2">
                    <!--Card-->
                    <div class="card hoverable">
                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <p class="card-text text-center">Operating System: </p>
                            <p class="card-block text-center">{{$ticket->os}}</p>
                            <hr>
                            <p class="card-text text-center">Issue summary: </p>
                            <p class="card-block text-center">{{$ticket->summary}}</p>
                            <hr>
                            <p class="card-text text-center">Comments:  </p>
                            @foreach($ticket->ticket_comments as $comment)
                                <p class="card-block text-center">{{$comment->comment}}</p><hr>
                            @endforeach

                            {!! Form::model($ticket, ['action' => 'TicketCommentController@store','route' => ['tickets.comments.store', $ticket->id]]) !!}
                            <div class="card-block text-center">
                                {!! Form::label('comment', 'New Comment') !!}
                                {!! Form::textarea('comment', null, array('placeholder' => 'Add New Comment','class' => 'form-control')) !!}
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-success" type="submit">Add Comment</button>
                            </div>
                            <div class="pull-left">
                                <a class="btn btn-primary" href="{{route('tickets.comments.index', $ticket)}}">Back to Comments</a>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
</main>