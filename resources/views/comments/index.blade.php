@extends('layouts/default')

<main>
    @section('content')
        <div class="container m-5">
            <div class="row">
                <div class="col-md-12 mt-5 text-center">
                    <div class="pull-left">

                        <h2>All Comments for Ticket {{$comments->first()->ticket_id}}</h2>
                        <h4 class="pull-left">(Status: {{$ticket->status}})</h4>
                    </div>
                </div>
                    <table class="table table-bordered">
                        <tr>
                            <th>Number</th>
                            <th>Comment</th>
                        </tr>
                        @foreach ($comments as $comment)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $comment->comment}}</td>
                            </tr>
                        @endforeach
                    </table>
                <hr>

            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <hr>
                    @if($ticket->status == 'Pending' | $ticket->status == 'In Progress')
                        <div class="pull-right">
                            <a class="btn btn-success align-content-center" href="comments/create">Add New Comment</a>
                        </div>
                        @else
                        <div class="pull-right">
                            <a class="btn btn-success disabled align-content-center" href="">Check Ticket Status</a>
                        </div>
                    @endif
                    <div class="pull-left">
                        <a class="btn btn-primary align-content-center" href="{{route('tickets.show', $comments->first()->ticket_id)}}">Back to Ticket {{$comments->first()->ticket_id}}</a>
                    </div>
                </div>
            </div>

            <div class="row">
                {!! $comments->links() !!}
            </div>

        </div>


</main>
