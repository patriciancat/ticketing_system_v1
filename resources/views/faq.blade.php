<section id="FAQ" class="text-center">
    <div class="container">
        <h3 class="flip"> How do I submit a support ticket? <b class="caret"></b></h3>
        <div class="panel">
            <p> 1. On the navigation menu, click "User Options", then "Create Ticket".</p>
            <p> 2. Input your details. </p>
            <p> 3. Click Submit Ticket. You should then be redirected to the Ticket Details page. </p>
            <br>
        </div>
        <h3 class="flip">How long does it usually take until my issue is resolved?<b class="caret"></b></h3>
        <div class="panel">
            <p> It depends on the problem. It can take from 1 day to 2 weeks. Tickets are typically resolved in a few
                days.</p>
        </div>
        <h3 class="flip">What if I resolved my issue and would like to cancel? <b class="caret"></b></h3>
        <div class="panel">
            <p> If you no longer need the support of IT Staff you can: </p>
            <p> 1. Search for your ticket under "User Options -> Search for ticket" </p>
            <p> 2. Input your ticket ID (You will have been notified of your ticket ID upon submission.</p>
            <p> 3. Add a comment to notify staff that problem is resolved and they will close your ticket.
                Alternatively, you can close your ticket yourself from this page. </p>
        </div>
    </div>
</section>