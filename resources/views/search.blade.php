@extends('layouts.default')
<main>
    @section('content')
        <div class="container m-5">
            <div class="row">
                <div class="col-md-12 mt-5 text-center">
                    <div class="text-center">
                        <h2>Search for a Ticket</h2>
                    </div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <div class="col-md-8 offset-md-2 full-height">

                    {!! Form::open(['action' => 'TicketController@search']) !!}

                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" name="search" placeholder="Input Ticket ID...">
                        <span class="input-group-btn">
                    <button class="btn btn-default-sm primary-color" type="submit">
                    <i class="fa fa-search primary-color"></i>
                    </button>
                    </span>
                    </div>
                    {!! Form::close() !!}


                </div>
            </div>
        </div>
</main>