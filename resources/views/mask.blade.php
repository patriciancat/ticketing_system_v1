<html>
    <header>
        <!--Mask-->
        <div class="view hm-black-light">
            <div class="full-bg-img flex-center">
                <div class="container">
                    <ul>
                        <li>
                            <h1 class="h1-responsive wow fadeInDown">ITS Ticketing System</h1>
                        </li>
                        <li>
                            <p class="wow fadeInDown" data-wow-delay="0.2s">Welcome to the ITS Ticket Submission system!
                                If you are experiencing difficulties with your IT system, please submit a ticket and the team will attempt to resolve your issue as soon as possible.</p>
                        </li>
                        <li>
                            <a href="" class="btn btn-primary btn-lg wow fadeInLeft" data-wow-delay="0.4s">Submit a ticket</a>
                            <a href="" class="btn btn-default btn-lg wow fadeInRight" data-wow-delay="0.4s">Learn more</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/.Mask-->
    </header>
</html>