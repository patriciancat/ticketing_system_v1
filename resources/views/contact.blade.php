<section id="contact">
    <div class="col-md-8 offset-2">
        <ul class="list-unstyled">
            <li class="mb-4">
                <i class="fa fa-2x mb-3 fa-map-marker"></i>
                <p>Melbourne, VIC 3000, Australia</p>
            </li>

            <li class="mb-4"><i class="fa fa-2x mb-3 fa-phone"></i>
                <p>(03) 9925 2000</p>
            </li>

            <li class="mb-4"><i class="fa fa-2x mb-3 fa-envelope"></i>
                <p>RMIT University, GPO Box 2476, Melbourne VIC 3001 Australia</p>
            </li>
        </ul>

    </div>
</section>